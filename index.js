const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("This is my express app");
});

app.get("/me", (req, res) => {
  res.send("Hi, this is Imran and here is my express app for you");
});

app.listen(5000, () => {
  console.log("listening");
});
